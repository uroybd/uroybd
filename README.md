# Utsob Roy
Programmer, Writer, and Thought Criminal.

[![Website: utsob.me](https://img.shields.io/badge/Website-utsob.me-blueviolet)](https://utsob.me)
[![Linkedin: thaianebraga](https://img.shields.io/badge/uroybdroybd-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/uroybd/)](https://www.linkedin.com/in/uroybd/)
[![Twitter: uroybd](https://img.shields.io/twitter/follow/uroybd?style=social)](https://twitter.com/uroybd)
[![Reddit!: uroybd](https://img.shields.io/reddit/user-karma/combined/uroybd?style=social)](https://www.reddit.com/user/uroybd)

---

I'm a self-taught programmer experienced in Python, TypeScript, Rust, Golang, and Clojure.

I take my writing with equal professionalism and trying to do the best I can.

**Thought Criminal** is a Orwellian term. My intended use is to describe me as a freethinker.

I enjoy reading. A lot.
